package com.coursach;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ServerInteraction {
    Socket socket;

    DataOutputStream out;

    DataInputStream in;

    public ServerInteraction () throws IOException {
        socket = new Socket("localhost", 3345);
        out = new DataOutputStream(socket.getOutputStream());
        in = new DataInputStream(socket.getInputStream());
    }

    protected void finalize() throws IOException {
        in.close();
        out.close();
        socket.close();
    }

    public String getMessage () throws IOException {
        return in.readUTF();
    }

    public void sendMessage (String message) throws IOException, InterruptedException {
        out.writeUTF(message);
        out.flush();
        Thread.sleep(1000);
    }
}
