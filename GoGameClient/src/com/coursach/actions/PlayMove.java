package com.coursach.actions;

import com.coursach.Main;
import com.coursach.ServerInteraction;
import com.coursach.game.Player;
import com.coursach.gui.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class PlayMove implements ActionListener {
    private int x,y;

    private GUI gui;

    private ServerInteraction serverInteraction;

    public PlayMove(GUI gui, int x, int y) {
        super();

        this.serverInteraction = gui.getServerInterraction();
        this.gui = gui;
        this.x = x;
        this.y = y;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(!gui.isWaitingServerAnswer()) {
            Player player = gui.getGoban().getPlayer();
            try {
                if (!gui.getGoban().play(x, y, player)) return;
            } catch (Exception ex) {
            }
            gui.getGoban().nextPlayer();
            gui.updateGoban();
            try {
                serverInteraction.sendMessage("" + x + y);
            } catch (IOException e1) {
                Main.showErrorMessageAndExit();
            } catch (InterruptedException e2) {
                Main.showErrorMessageAndExit();
            }
            new ServerMove(gui).execute();
        }
    }
}
