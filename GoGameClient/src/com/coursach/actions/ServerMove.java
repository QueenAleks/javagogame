package com.coursach.actions;

import com.coursach.Main;
import com.coursach.gui.GUI;

import javax.swing.*;
import java.io.IOException;

public class ServerMove extends SwingWorker<String, String> {
    private GUI gui;

    public ServerMove(GUI gui){
        this.gui = gui;
    }

    protected String doInBackground() {
        String answer = "";
        try {
            gui.setWaitingServerAnswer();
            answer = gui.getServerInterraction().getMessage();
            gui.unsetWaitingServerAnswer();
        } catch (IOException e) {
            Main.showErrorMessageAndExit();
        }
        return answer;
    }

    protected void done(){
        String answer = "";
        try {
            answer = get();
        } catch (Exception e){
        }
        if (answer.equals("error")){
            Main.showErrorMessageAndExit();
        }
        if (answer.equals("end")){
            String score = "";
            try {
                score = gui.getServerInterraction().getMessage();
            } catch (IOException e){
                Main.showErrorMessageAndExit();
            }
            JOptionPane.showMessageDialog(gui, score);
            System.exit(0);
        }
        if (answer.equals("pass")){
            gui.getGoban().pass(gui.getGoban().getPlayer());
            gui.updateGoban();
            return;
        }
        int x = Integer.parseInt(answer) / 10,
            y = Integer.parseInt(answer) % 10;
        try {
            gui.getGoban().play(x, y, gui.getGoban().getPlayer());
        } catch (Exception e){
        }
        gui.getGoban().nextPlayer();
        gui.updateGoban();
    }
}
