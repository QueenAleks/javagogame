package com.coursach.actions;

import com.coursach.Main;
import com.coursach.ServerInteraction;
import com.coursach.game.Goban;
import com.coursach.gui.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class PassMove implements ActionListener {
    private GUI gui;

    private ServerInteraction serverInteraction;

    public PassMove(GUI gui) {
        this.serverInteraction = gui.getServerInterraction();
        this.gui = gui;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Goban goban = gui.getGoban();
        if(!gui.isWaitingServerAnswer()) {
            try {
                serverInteraction.sendMessage("pass");
            } catch (IOException e1) {
                Main.showErrorMessageAndExit();
            } catch (InterruptedException e1) {
                Main.showErrorMessageAndExit();
            }
            goban.pass(goban.getPlayer());
            gui.updateGoban();
            new ServerMove(gui).execute();
        }
    }
}