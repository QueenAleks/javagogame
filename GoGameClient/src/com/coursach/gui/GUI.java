package com.coursach.gui;

import com.coursach.*;
import com.coursach.actions.PassMove;
import com.coursach.actions.PlayMove;
import com.coursach.actions.ServerMove;
import com.coursach.game.Goban;
import com.coursach.game.StoneChain;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class GUI extends JFrame {
    public static final int TOKEN_INITIAL_SIZE = 35;
    private static final int MENU_SIZE = 42;
    private static final int GOBAN_SIZE = 9;

    private Goban goban;

    private String name;
    private String opponentName;

    private static final int height = TOKEN_INITIAL_SIZE * GOBAN_SIZE + MENU_SIZE;
    private static final int width = TOKEN_INITIAL_SIZE * GOBAN_SIZE;

    private JMenuBar jMenuBar;
    private JMenu jMenuGame;
    private JMenuItem jGameNew;

    private JMenu jMenuPass;
    private JMenuItem jPassPass;

    private JMenu jMenuHelp;
    private JMenuItem jHelpHelp;

    private JPanel jGoban;
    private JButton[][] jIntersections;

    private ServerInteraction serverInteraction;

    private boolean isWaitingServerAnswer;

    public boolean isBlack;

    public GUI(Goban goban, ServerInteraction serverInteraction, boolean isBlack, String name, String opponentName) throws IOException {
        this.serverInteraction = serverInteraction;
        this.isWaitingServerAnswer = false;

        this.name = name;
        this.opponentName = opponentName;

        this.goban = goban;

        this.initMenu();

        setJMenuBar(jMenuBar);

        this.initWindow();

        this.drawGoban();

        pack();

        this.isBlack = isBlack;

        if (!isBlack){
            new ServerMove(this).execute();
        }
    }

    public Goban getGoban() {
        return goban;
    }

    public ServerInteraction getServerInterraction() { return serverInteraction; }

    public boolean isWaitingServerAnswer() { return isWaitingServerAnswer; }

    public void setWaitingServerAnswer() { isWaitingServerAnswer = true; }

    public void unsetWaitingServerAnswer() { isWaitingServerAnswer = false; }

    private void initWindow() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Java Go game: " + name + " vs " + opponentName);
        setPreferredSize(new Dimension(width, height));
        setResizable(false);
        setVisible(true);
    }

    protected void initMenu() {
        jMenuBar = new JMenuBar();

        jMenuGame = new JMenu("Game");

        jGameNew = new JMenuItem("New");
        jGameNew.setAccelerator(KeyStroke.getKeyStroke("control N"));
        jGameNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Main.newGame();
                    // Закрываем текущее окно
                    GUI.this.setVisible(false);
                    GUI.this.dispose();
                } catch (Exception ex) {
                }
            }
        });

        jMenuPass = new JMenu("Pass");
        jPassPass = new JMenuItem("Pass");
        jPassPass.setAccelerator(KeyStroke.getKeyStroke("control P"));
        jPassPass.setEnabled(true);
        jPassPass.addActionListener(new PassMove(this));

        jMenuHelp = new JMenu("Help");
        jHelpHelp = new JMenuItem("GameRules");
        jHelpHelp.setAccelerator(KeyStroke.getKeyStroke("control H"));
        jHelpHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(GUI.this, "Game Rules (TODO)", "Rules", JOptionPane.PLAIN_MESSAGE);
            }
        });

        jMenuGame.add(jGameNew);
        jMenuPass.add(jPassPass);
        jMenuHelp.add(jHelpHelp);

        jMenuBar.add(jMenuGame);
        jMenuBar.add(jMenuPass);
        jMenuBar.add(jMenuHelp);
    }

    public void drawGoban() {

        jGoban = new JPanel(new GridLayout(GOBAN_SIZE,GOBAN_SIZE));

        jIntersections = new JButton[GOBAN_SIZE][GOBAN_SIZE];

        // Creating the buttons
        for(int x=0;x<GOBAN_SIZE;x++) {
            for(int y=0;y<GOBAN_SIZE;y++) {

                StoneChain chain = goban.getIntersection(x, y).getStoneChain();
                if (chain != null) {
                    jIntersections[x][y] = new JButton(Sprite.getPlayerIcon(chain.getOwner().getIdentifier()));
                } else {
                    jIntersections[x][y] = new JButton(Sprite.getGridIcon(goban, x, y, 0));
                }

                jIntersections[x][y].setEnabled(true);
                jIntersections[x][y].setBorder(BorderFactory.createEmptyBorder());
                jIntersections[x][y].setContentAreaFilled(false);

                jIntersections[x][y].setPreferredSize(new java.awt.Dimension(TOKEN_INITIAL_SIZE, TOKEN_INITIAL_SIZE));

                // Adding action
                jIntersections[x][y].addActionListener(new PlayMove(this, x, y));
                jIntersections[x][y].addMouseListener(new HoverEffect(x, y, jIntersections[x][y],this));

                // Adding button
                jGoban.add(jIntersections[x][y],x,y);
            }
        }
        getContentPane().add(jGoban, BorderLayout.CENTER);
    }

    public void updateGoban() {
        for(int x=0;x<GOBAN_SIZE;x++) {
            for (int y = 0; y < GOBAN_SIZE; y++) {
                StoneChain sc = goban.getIntersection(x,y).getStoneChain();
                if (sc != null) {
                    jIntersections[x][y].setIcon(Sprite.getPlayerIcon(sc.getOwner().getIdentifier()));
                } else {
                    jIntersections[x][y].setIcon(Sprite.getGridIcon(goban, x, y, 0));
                }
            }
        }
    }
}
