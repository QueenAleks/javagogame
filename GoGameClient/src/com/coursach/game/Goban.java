package com.coursach.game;

import com.coursach.game.exceptions.OutOfGobanException;

import java.util.HashSet;
import java.util.Set;

/**
 * Goban is a class containing the board of the game, this abstract board also handles the basic rules of the game.
 */
public class Goban {
    private final int SIZE = 9;

    private final Intersection[][] intersections;

    private Player P1, P2, actualPlayer;

    private int successivePassCount;

    public Goban() {
        this.successivePassCount = 0;
        this.intersections = new Intersection[SIZE][SIZE];
        initGoban();
    }

    private void initGoban() {
        // Initializing players
        P1 = new Player(1);
        P2 = new Player(2);
        actualPlayer = P1;

        for (int x = 0; x < this.SIZE; x++) {
            for (int y = 0; y < this.SIZE; y++) {
                intersections[x][y] = new Intersection(this, x, y);
            }
        }
    }

    public int getSize() {
        return SIZE;
    }

    public boolean isInGoban(int x, int y) {
        return (x >= 0 && x < SIZE && y >= 0 && y < SIZE);
    }

    public boolean isInGoban(Intersection intersection) {
        int x = intersection.getX();
        int y = intersection.getY();
        return (x >= 0 && x < SIZE && y >= 0 && y < SIZE);
    }

    public Intersection getIntersection(int x, int y) {
        if (isInGoban(x, y)) {
            return intersections[x][y];
        } else {
            return null;
        }
    }

    public void pass(Player player) {
        nextPlayer();
    }

    public boolean play(Intersection intersection, Player player) {
        // Should be in goban
        if (!isInGoban(intersection)) return false;

        // Preventing playing over another stone
        if (intersection.getStoneChain() != null) return false;

        Set<Intersection> capturedStones = null;
        Set<StoneChain> capturedStoneChains = null;

        Set<StoneChain> adjStoneChains = intersection.getAdjacentStoneChains();
        StoneChain newStoneChain = new StoneChain(intersection, player);
        intersection.setStoneChain(newStoneChain);
        for (StoneChain stoneChain : adjStoneChains) {
            if (stoneChain.getOwner() == player) {
                newStoneChain.add(stoneChain, intersection);
            } else {
                stoneChain.removeLiberty(intersection);
                if (stoneChain.getLiberties().size() == 0) {
                    stoneChain.die();
                }
            }
        }

        // Preventing suicide and re-adding liberty
        if (newStoneChain.getLiberties().size() == 0) {
            for (StoneChain chain : intersection.getAdjacentStoneChains()) {
                chain.getLiberties().add(intersection);
            }
            intersection.setStoneChain(null);
            return false;
        }

        // Move is valid, applying changes
        for (Intersection stone : newStoneChain.getStones()) {
            stone.setStoneChain(newStoneChain);
        }

        return true;
    }

    public boolean play(int x, int y, Player player) throws OutOfGobanException {
        Intersection intersection = getIntersection(x, y);
        if (intersection == null) {
            throw new OutOfGobanException("Intersection is out of range: x=" + x + " y=" + y);
        }
        return play(intersection, player);
    }

    public Player getPlayer() {
        return actualPlayer;
    }

    public boolean nextPlayer() {
        if (actualPlayer == P1) {
            actualPlayer = P2;
        } else {
            actualPlayer = P1;
        }
        return true;
    }
}
