package com.coursach.game.exceptions;

public class OutOfGobanException extends Exception {
    public OutOfGobanException() {
    }

    public OutOfGobanException(String message) {
        super(message);
    }

    public OutOfGobanException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutOfGobanException(Throwable cause) {
        super(cause);
    }

    public OutOfGobanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
