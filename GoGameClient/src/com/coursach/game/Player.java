package com.coursach.game;

/**
 * Player is a class describing the players which may play the game
 */
public class Player {
    private final int identifier;

    private int capturedStones;

    public Player(int identifier) {
        this.identifier = identifier;
        this.capturedStones = 0;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void addCapturedStones(int nb) {
        capturedStones += nb;
    }

    public boolean play(Goban goban, int x, int y) {
        if (x == -1 && y == -1) {
            return true;
        } else {
            return goban.play(goban.getIntersection(x,y),this);
        }
    }

    @Override
    public String toString() {
        return "Player "+identifier;
    }
}
