package com.coursach.game;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a stone chain, of horizontally and vertically connected stones of a same color
 * It also keeps track of the horizontal and vertical vacancies;
 */
public class StoneChain {
    private final Set<Intersection> stones;

    private final Set<Intersection> liberties;

    private final Player owner;

    public StoneChain(Set<Intersection> stones, Set<Intersection> liberties, Player owner) {
        this.stones = stones;
        this.liberties = liberties;
        this.owner = owner;
    }

    public StoneChain(Intersection intersection, Player owner) {
        stones = new HashSet<Intersection>();
        stones.add(intersection);
        this.owner = owner;
        liberties = new HashSet<Intersection>(intersection.getEmptyNeighbors());
    }

    public StoneChain(StoneChain stoneChain) {
        this.stones = new HashSet<Intersection>(stoneChain.stones);
        this.liberties = new HashSet<Intersection>(stoneChain.liberties);
        this.owner = stoneChain.owner;
    }

    public Player getOwner() {
        return owner;
    }

    public Set<Intersection> getLiberties() {
        return liberties;
    }

    public Set<Intersection> getStones() {
        return stones;
    }

    public void add(StoneChain stoneChain, Intersection playedStone) {
        // Fuse stone sets
        this.stones.addAll(stoneChain.stones);
        // Fuse liberties
        this.liberties.addAll(stoneChain.liberties);
        // remove played stone from liberties
        this.liberties.remove(playedStone);
    }

    public StoneChain removeLiberty(Intersection playedStone) {
        StoneChain newStoneChain = new StoneChain(stones, liberties, owner);
        newStoneChain.liberties.remove(playedStone);
        return newStoneChain;
    }

    public void die() {
        for (Intersection rollingStone : this.stones) {
            rollingStone.setStoneChain(null);
            Set<StoneChain> adjacentStoneChains = rollingStone.getAdjacentStoneChains();
            for (StoneChain stoneChain : adjacentStoneChains) {
                stoneChain.liberties.add(rollingStone);
            }
        }
        this.owner.addCapturedStones(this.stones.size());
    }
}
