package com.coursach;

import com.coursach.gui.GUI;
import com.coursach.game.Goban;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        newGame();
    }

    public static void showErrorMessageAndExit() {
        JDialog dialog = new JDialog((Dialog) null, "Failed to connect to server!", true);
        dialog.setSize(360, 90);
        dialog.setVisible(true);
        System.exit(0);
    }

    public static void showErrorMessageAndExit(String message) {
        JDialog dialog = new JDialog((Dialog) null, message, true);
        dialog.setSize(360, 90);
        dialog.setVisible(true);
        System.exit(0);
    }

    public static void newGame() {
        String name = "";
        String opponentName = "";
        while (name.equals("")){
            name = JOptionPane.showInputDialog(null, "Hello! What is your name?");
            if (name == null){
                System.exit(0);
            }
        }
        String[] choises = {"Create Game", "Join Game"};
        String mode = (String) JOptionPane.showInputDialog(null, " ", "New Game",
                                                           JOptionPane.QUESTION_MESSAGE, null, choises, choises[0]);
        if (mode == null){
            System.exit(0);
        }


        boolean isNewGame = true;
        ServerInteraction serverInteraction = null;
        try {
            serverInteraction = new ServerInteraction();
            serverInteraction.sendMessage(name);
            if (mode.equals("Create Game")){
                serverInteraction.sendMessage("CreateGame");
                Thread.sleep(100);
                if (serverInteraction.getMessage().equals("GameExists")){
                    showErrorMessageAndExit("Game Already Exists");
                }
                isNewGame = true;
                JDialog dialog = new JDialog((Dialog) null, "Waiting for another player.", false);
                dialog.setSize(360, 90);
                dialog.setVisible(true);
                if (serverInteraction.getMessage().equals("start")) {
                    dialog.dispose();
                }
            } else {
                serverInteraction.sendMessage("JoinGame");
                Thread.sleep(100);
                if (serverInteraction.getMessage().equals("NoGames")){
                    showErrorMessageAndExit("No Games On Server");
                }
                isNewGame = false;
            }
            opponentName = serverInteraction.getMessage();
        } catch (IOException e){
            showErrorMessageAndExit();
        } catch (Exception e){
            e.printStackTrace();
        }

        Goban goban = new Goban();
        try {
            GUI gui = new GUI(goban, serverInteraction, isNewGame, name, opponentName);
        } catch (IOException e) {
            showErrorMessageAndExit();
        }
    }
}
