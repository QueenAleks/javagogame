package com.coursach;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

public class Logger {
    static void log (String message, String filename){
        message = LocalDateTime.now().toString() + " " + message;
        System.out.println(message);
        try (FileWriter f = new FileWriter(filename, true);){
            f.write(message);
            f.append('\n');
            f.flush();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void serverLog (String message){
        log(message, "server.log");
    }

    public static void gameLog (String message){
        log(message, "game.log");
    }
}
