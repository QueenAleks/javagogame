package com.coursach;

import com.coursach.core.Goban;
import com.coursach.core.StoneChain;
import com.coursach.score.Scorer;

import java.io.IOException;

public class Game {
    Goban goban;

    Scorer scorer;

    int[] score;

    ClientInterraction blackPlayer;

    ClientInterraction whitePlayer;

    boolean passed;

    public Game(ClientInterraction blackPlayer, ClientInterraction whitePlayer){
        this.blackPlayer = blackPlayer;
        this.whitePlayer = whitePlayer;
        goban = new Goban();
        scorer = new Scorer(goban);
        passed = false;
    }

    String makeMove(String turn){
        if (turn.equals("pass")){
            if (passed){
                scorer.init();
                updateScore(null);
                return "end";
            } else {
                goban.pass(goban.getPlayer());
                passed = true;
                return turn;
            }
        } else {
            passed = false;
        }
        int x = Integer.parseInt(turn) / 10,
            y = Integer.parseInt(turn) % 10;
        goban.play(x, y, goban.getPlayer());
        goban.nextPlayer();

        return turn;
    }

    void updateScore(StoneChain chain) {
        scorer.flipDeathStatus(chain);
    }

    void countScore(){
        scorer = new Scorer(goban);
        scorer.init();
        score = scorer.outputScore();
    }

    String getFinalScore(){
        score = scorer.outputScore();
        double Wscore = (double) (score[1]-score[0]) + 6.5;

        return "The number of stones taken from " + blackPlayer.name + " is: " + goban.getPlayer(1).getCapturedStones() + "\n" +
               "The number of stones taken from " + whitePlayer.name + " is: " + goban.getPlayer(2).getCapturedStones() + "\n\n" +
               "The Komi is set a a 6.5 bonus for White\n\n" +
               "The final score is:\n\t" + blackPlayer.name + " " + score[0] + "\n\t" + whitePlayer.name + " " + ((double) score[1] + 6.5) + "\n" +
               (Wscore>0?"White":"Black") + " wins by " + Math.abs(Wscore) + " Points.\n";
    }

    public void play () throws IOException {
        Logger.gameLog(blackPlayer.name + " vs " + whitePlayer.name + " start playing!");
        int count = 1;
        while (true){
            String answer = makeMove(blackPlayer.getMessage());
            whitePlayer.sendMessage(answer);
            if (answer.equals("end")) {
                blackPlayer.sendMessage(answer);
                break;
            }
            countScore();
            Logger.gameLog(blackPlayer.name + " turn: " + count + "; black stone on point " + answer + "; " + blackPlayer.name + ": " + score[0] + "; " + whitePlayer.name + ": " + score[1] + ";");
            count++;
            answer = makeMove(whitePlayer.getMessage());
            blackPlayer.sendMessage(answer);
            if (answer.equals("end")) {
                whitePlayer.sendMessage(answer);
                break;
            }
            countScore();
            Logger.gameLog(whitePlayer.name + " turn: " + count + "; white stone on point " + answer + "; " + blackPlayer.name + ": " + score[0] + "; " + whitePlayer.name + ": " + score[1] + ";");
            count++;
        }

        String finalScore = getFinalScore();
        blackPlayer.sendMessage(finalScore);
        whitePlayer.sendMessage(finalScore);
        Logger.gameLog("Game Over! " + blackPlayer.name + ": " + score[0] + ". " + whitePlayer.name + ": " + ((double)score[1] + 6.5) + ".");
    }
}
