package com.coursach;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientInterraction {
    public Socket socket;

    DataOutputStream out;

    DataInputStream in;

    public String name;

    public ClientInterraction(Socket serverAccept) throws IOException {
        socket = serverAccept;
        out = new DataOutputStream(socket.getOutputStream());
        in = new DataInputStream(socket.getInputStream());
    }

    protected void finalize() throws IOException {
        in.close();
        out.close();
        socket.close();
    }

    public String getMessage () throws IOException {
        return in.readUTF();
    }

    public void sendMessage (String message) throws IOException {
        out.writeUTF(message);
        out.flush();
    }
}
