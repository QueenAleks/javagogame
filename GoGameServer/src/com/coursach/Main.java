package com.coursach;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {
    public static void main(String[] args) {
        System.out.println("Start server on port 3345");
        while (true) {
            ClientInterraction blackPlayer = null, whitePlayer = null;
            try (ServerSocket server = new ServerSocket(3345)) {
                blackPlayer = new ClientInterraction(server.accept());
                blackPlayer.name = blackPlayer.getMessage();
                String action = blackPlayer.getMessage();
                if (action.equals("CreateGame")){
                    Logger.serverLog(blackPlayer.name + " create game. Waiting for another player");
                    blackPlayer.sendMessage("OK.Wait");
                } else if (action.equals("JoinGame")){
                    Logger.serverLog(blackPlayer.name + " tried to join empty game");
                    blackPlayer.sendMessage("NoGames");
                    continue;
                }
                boolean connected = false;
                while (!connected){
                    whitePlayer = new ClientInterraction(server.accept());
                    whitePlayer.name = whitePlayer.getMessage();
                    action = whitePlayer.getMessage();
                    if (action.equals("CreateGame")){
                        Logger.serverLog(whitePlayer.name + " tried to create game. Game already exists.");
                        whitePlayer.sendMessage("GameExists");
                    } else if (action.equals("JoinGame")){
                        Logger.serverLog(whitePlayer.name + " connected to game.");
                        connected = true;
                        Thread.sleep(1000);
                        blackPlayer.sendMessage("start");
                        whitePlayer.sendMessage("start");
                        Thread.sleep(100);
                        blackPlayer.sendMessage(whitePlayer.name);
                        whitePlayer.sendMessage(blackPlayer.name);
                        Logger.serverLog(blackPlayer.name + " and " + whitePlayer.name + " start the game");
                        new Game(blackPlayer, whitePlayer).play();
                        Logger.serverLog("End Game. Waiting another players.");
                    }
                }
            } catch (IOException e) {
                try {
                    if (blackPlayer.socket.isConnected()) {
                        blackPlayer.sendMessage("error");
                    } else if (whitePlayer.socket.isConnected()){
                        whitePlayer.sendMessage("error");
                    }
                } catch (Exception eee) {}
                Logger.serverLog("Error - Client Disconnected. Game Stopped.");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}