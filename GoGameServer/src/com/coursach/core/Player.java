package com.coursach.core;

import java.util.Collections;

/**
 * ClientInterraction is a class describing the players which may play the game
 * Created by Thomas on 12/4/2014.
 */
public class Player {
    private final int identifier;

    private int capturedStones;

    public Player(int identifier) {
        this.identifier = identifier;
        this.capturedStones = 0;
    }

    public int getIdentifier() {
        return identifier;
    }

    public int getCapturedStones() {
        return capturedStones;
    }

    public void addCapturedStones(int nb) {
        capturedStones += nb;
    }

    public void removeCapturedStones(int nb) { capturedStones -= nb; }

    public void play(Goban goban, int x, int y) {
        goban.play(x, y,this);
    }

    @Override
    public String toString() {
        return "ClientInterraction "+identifier;
    }
}
